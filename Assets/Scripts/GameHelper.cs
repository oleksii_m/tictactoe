﻿using UnityEngine;
using System.Collections;
using StateMachine;

public interface IGameState
{
    TurnPlayers TurnPlayers { get; set; }
    BoardController BoardController { get; }

}

public enum Difficulty
{
    None = 0,
    Easy,
    Normal,
    Hard
}

public class GameHelper : MonoBehaviour, IGameState
{
    private const int ROWS = 3;
    private const int COLS = 3;

    public TurnPlayers TurnPlayers { get { return _turn; } set { _turn = value; } }
    BoardController IGameState.BoardController { get { return _boardController; } }

    [SerializeField]
    private BoardView _boardView;

    private BoardController _boardController;

    private TurnPlayers _turn;
    private StateMachine<IGameState> _stateMachine;

    void Start()
    {
        _boardController = new BoardController(new BoardModel(COLS, ROWS), _boardView);

        _stateMachine = new StateMachine<IGameState>(this, new GameLoadState());
        _stateMachine.addState(new GamePlayState());
        _stateMachine.addState(new GameOverState());
        _stateMachine.CurrentState.Begin();
    }

    private void OnDestroy()
    {
        _boardController.GetInputControler().DetatchTileView();

    }
}
