﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;


public abstract class PlayerPrefsValue<T>
{
    protected string _key;
    protected T _defaultValue;

    public PlayerPrefsValue(string key, T defaultValue)
    {
        this._key = key;
        this._defaultValue = defaultValue;
    }

    public abstract T Value
    {
        get;
        set;
    }

    public abstract T GetDefaultValue();
}

public class PlayerPrefsFloat : PlayerPrefsValue<float>
{
    public PlayerPrefsFloat(string key, float defaultValue) : base(key, defaultValue)
    {

    }

    public override float Value
    {
        get { return PlayerPrefs.GetFloat(_key, _defaultValue); }
        set { PlayerPrefs.SetFloat(_key, value); PlayerPrefs.Save(); }
    }

    public override float GetDefaultValue()
    {
        return _defaultValue;
    }
}

public class PlayerPrefsInt : PlayerPrefsValue<int>
{

    public PlayerPrefsInt(string key, int defaultValue) : base(key, defaultValue)
    {

    }

    public override int Value
    {
        get { return PlayerPrefs.GetInt(_key, _defaultValue); }
        set { PlayerPrefs.SetInt(_key, value); PlayerPrefs.Save(); }
    }

    public override int GetDefaultValue()
    {
        return _defaultValue;
    }
}

public class PlayerPrefsString : PlayerPrefsValue<string>
{
    public PlayerPrefsString(string key, string defaultValue) : base(key, defaultValue)
    {

    }

    public override string Value
    {
        get { return PlayerPrefs.GetString(_key, _defaultValue); }
        set { PlayerPrefs.SetString(_key, value); PlayerPrefs.Save(); }
    }

    public override string GetDefaultValue()
    {
        return _defaultValue;
    }
}

public class PlayerPrefsBool : PlayerPrefsValue<bool>
{
    public PlayerPrefsBool(string key, bool defaultValue) : base(key, defaultValue)
    {
    }

    public override bool Value
    {
        get { return ConvertIntToBool( PlayerPrefs.GetInt(_key, ConvertBoolToInt(_defaultValue))); }
        set { PlayerPrefs.SetInt(_key, ConvertBoolToInt(value)); PlayerPrefs.Save(); }
    }

    public override bool GetDefaultValue()
    {
        return _defaultValue;
    }

    private int ConvertBoolToInt(bool value)
    {
        return value ? 1 : 0;
    }

    private bool ConvertIntToBool(int value)
    {
        return value == 1;
    }
}

public class GameResult
{
    private string _key = "";
    private int _defaultValue = 0;
    private PlayerPrefsInt _result;

    public GameResult (string key, int defaultValue)
    {
        this._key = key;
        this._defaultValue = defaultValue;
    }

    public int GetResult(int difficulty)
    {
        _result = new PlayerPrefsInt(this._key + difficulty, this._defaultValue);

        return _result.Value;
    }

    public void IncResult(int difficulty)
    {
        _result = new PlayerPrefsInt(this._key + difficulty, this._defaultValue);
        _result.Value++;
    }
}

public class Storage : MonoBehaviour
{
    private static Storage _instance;
    public static Storage Instance
    {
        get
        {
            if (_instance == null)
            {
                _instance = FindObjectOfType<Storage>();
            }
            return _instance;
        }
    }

    void Awake()
    {
        _instance = this;
    }

    private PlayerPrefsInt _difficulty = new PlayerPrefsInt("difficulty", 3);
    private GameResult _wins = new GameResult("wins", 0);
    private GameResult _losses = new GameResult("losses", 0);
    private GameResult _draws = new GameResult("draws", 0);

    public int Difficulty { get { return _difficulty.Value; } set { _difficulty.Value = value; } }
    public GameResult Wins { get { return _wins; } }
    public GameResult Losses { get { return _losses; } }
    public GameResult Draws { get { return _draws; } }

}