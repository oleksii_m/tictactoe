﻿using UnityEngine;
using System.Collections;
using System;

public static class GameEvent
{
    public static event Action<TileState> OnGameOver;
    public static event Action OnGameReload;
    public static event Action OnGameLoad;

    public static event Action<TileState> OnPlayerTurnChange;

    private static TileState _currentPlayerTurn;

    public static void GameOver(TileState player)
    {
        if (OnGameReload != null)
            OnGameOver(player);
    }

    public static void ReloadGame()
    {
        if (OnGameReload != null)
            OnGameReload();
    }

    public static void LoadGame()
    {
        if (OnGameReload != null)
            OnGameLoad();
    }

    public static void SetCurrentPlayerTurn(TileState turn)
    {
        if (OnPlayerTurnChange != null)
            OnPlayerTurnChange(turn);
    }

    public static void ClearGameReload()
    {
        if (OnGameReload == null)
            return;

        foreach (var item in OnGameReload.GetInvocationList())
        {
            OnGameReload -= (Action)item;
        }
    }

}
