﻿using UnityEngine;
using System.Collections;

public class MainPageHelper : MonoBehaviour
{
    [SerializeField]
    MainPageView _mainView;
    MainPageController _mainContr;

    private void Awake()
    {
        _mainContr = new MainPageController(_mainView);
    }

}
