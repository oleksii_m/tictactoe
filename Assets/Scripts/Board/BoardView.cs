﻿using UnityEngine;
using System.Collections;
using System;
using UnityEngine.UI;

public class BoardView : MonoBehaviour, IBoardView
{
    [SerializeField]
    private GridLayoutGroup _gridGroup;
    [SerializeField]
    private TileView _tilePrefab;

    private TileView[,] _tiles;

    private IInputControler _inputControler;

    public void Initialized(int cols, int rows)
    {
        this._tiles = new TileView[cols, rows];

        UpdateGridGroup(_gridGroup.gameObject, cols);
        DestroyAllCield(_gridGroup.gameObject);

        for (var col = 0; col < cols; col++)
        {
            for (var row = 0; row < rows; row++)
            {
                var tile = Instantiate<TileView>(this._tilePrefab);
                tile.transform.SetParent(this._gridGroup.transform, false);
                tile.Initialized(new Tile(col, row, TileState.Empty));
                this._tiles[col, row] = tile;
            }
        }

        _inputControler = new InputControler(_tiles);
        _inputControler.AttachTileView();
    }

    public TileView[,] GetTilesView()
    {
        return _tiles;
    }

    public void UpdateTilesState(Tile[,] tiles)
    {
        foreach (var tile in tiles)
        {
            UpdateTileState(tile);
        }
    }

    public void UpdateTileState(Tile tile)
    {
        _tiles[tile.Col, tile.Row].UpdateView(tile.State);
    }
    public IInputControler GetInputControler()
    {
        return _inputControler;
    }
    private void DestroyAllCield(GameObject obj)
    {
        for (var i = 0; i < obj.transform.childCount; i++)
        {
            var child = obj.transform.GetChild(i).gameObject;
            Destroy(child);
        }
    }

    private void UpdateGridGroup(GameObject board, int cols)
    {
        var gridGroup = board.GetComponent<GridLayoutGroup>();
        gridGroup.constraint = GridLayoutGroup.Constraint.FixedColumnCount;
        gridGroup.childAlignment = TextAnchor.MiddleCenter;
        gridGroup.constraintCount = cols;
    }


}
