﻿using UnityEngine;
using System.Collections;
using System;
using UnityEngine.UI;

[Serializable]
public struct TileStyle
{
    public TileState State;
    public Sprite Icon;
    public Color Color;
}
