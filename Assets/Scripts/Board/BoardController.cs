﻿using UnityEngine;
using System.Collections;
using System;

public interface IBoardModel
{
    int Rows { get; }
    int Cols { get; }

    Tile[,] Tiles { get; }

    void ClearAllTiles();
    void SetTileState(Tile tile);
    bool IsGameOver();
    bool HasWinner();

    TileState GetWinerPlayerType();
}

public interface IBoardView
{
    void Initialized(int cols, int rows);
    void UpdateTileState(Tile tile);
    void UpdateTilesState(Tile[,] tiles);
    IInputControler GetInputControler();
    TileView[,] GetTilesView();
}

public class BoardController
{
    public int Cols { get { return _model.Cols; } }
    public int Rows { get { return _model.Rows; } }

    private IBoardModel _model;
    private IBoardView _view;

    public BoardController(IBoardModel model, IBoardView view)
    {
        this._model = model;
        this._view = view;

        _view.Initialized(_model.Cols, _model.Rows);
        Clear();
    }

    public void SetTileState(Tile tile)
    {
        _model.SetTileState(tile);
        _view.UpdateTileState(tile);

    }

    public void Clear()
    {
        _model.ClearAllTiles();
        _view.UpdateTilesState(_model.Tiles);
    }

    public bool IsGameOver()
    {
        return _model.IsGameOver();
    }

    public bool HasWinner()
    {
        return _model.HasWinner();
    }

    public TileState GetWinerPlayerType()
    {
        return _model.GetWinerPlayerType();
    }

    public Tile[,] GetTiles()
    {
        return _model.Tiles;
    }


    public IInputControler GetInputControler()
    {
        return _view.GetInputControler();
    }
}
