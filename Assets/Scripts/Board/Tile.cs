﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using StateMachine;

public enum TileState
{
    None = 0,
    Empty,
    Circle,
    Cross
}

public class Tile
{
    public int Row { get { return _row; } }
    public int Col { get { return _col; } }
    public TileState State { get { return _state; } set { _state = value; } }

    private int _row;
    private int _col;
    private TileState _state;

    public Tile(int col, int row, TileState state = TileState.None)
    {
        this._col = col;
        this._row = row;
        this._state = state;
    }

    public bool IsEmpty()
    {
        return _state == TileState.Empty;
    }

    public bool IsNullOrEmpty()
    {
        return _state == TileState.None || IsEmpty();
    }
}
