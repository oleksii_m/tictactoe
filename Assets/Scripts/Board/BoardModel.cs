﻿using UnityEngine;
using System.Collections;
using System;

public class BoardModel : IBoardModel
{
    public int Cols { get { return _cols; } }
    public int Rows { get { return _rows; } }

    public Tile[,] Tiles { get { return _tiles; } }

    private int _rows;
    private int _cols;

    private Tile[,] _tiles;

    private int _emptyTileCount;
    public BoardModel(int cols, int rows)
    {
        this._cols = cols;
        this._rows = rows;

        ClearAllTiles();
    }

    public void ClearAllTiles()
    {
        _tiles = new Tile[_cols, _rows];

        for (var col = 0; col < _cols; col++)
        {
            for (var row = 0; row < _rows; row++)
            {
                _tiles[col, row] = new Tile(col, row, TileState.Empty);
            }
        }

        _emptyTileCount = _cols * _rows;
    }

    public TileState GetWinerPlayerType()
    {
        var winTile = Resources.Instance.GetWinTileCombinations();

        for (var col = 0; col < winTile.GetLength(0); col++)
        {
            var win = true;
            var startTile = Tiles[(int)winTile[col, 0].x, (int)winTile[col, 0].y];

            if (startTile.IsNullOrEmpty())
                continue;

            for (var row = 1; row < winTile.GetLength(1); row++)
            {
                win &= Tiles[(int)winTile[col, row].x, (int)winTile[col, row].y].State == startTile.State;
            }

            if (win)
                return startTile.State;

        }

        return TileState.None;
    }

    public bool IsGameOver()
    {
        return isDraw() || HasWinner();
    }



    public void SetTileState(Tile tile)
    {
        _tiles[tile.Col, tile.Row].State = tile.State;

        _emptyTileCount--;
    }

    private bool isDraw()
    {
        return _emptyTileCount == 0;
    }

    public bool HasWinner()
    {
        var winer = GetWinerPlayerType();

        return winer != TileState.None && winer != TileState.Empty;
    }
}
