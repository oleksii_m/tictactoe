﻿using UnityEngine;
using System.Collections;
using System;

public interface IInputControler
{
    event Action<Tile> OnTilesClicked;
    void AttachTileView();
    void DetatchTileView();
}

public class InputControler : IInputControler
{
    private TileView[,] _tiles;

    public InputControler(TileView[,] tiles)
    {
        this._tiles = tiles;
    }

    public event Action<Tile> OnTilesClicked;

    public void AttachTileView()
    {
        foreach (var tile in _tiles)
        {
            tile.OnClicked += TilesClecked;
        }
    }

    public void DetatchTileView()
    {
        foreach (var tile in _tiles)
        {
            tile.OnClicked -= TilesClecked;
        }
    }

    private void TilesClecked(Tile tile)
    {
        if (OnTilesClicked != null)
            OnTilesClicked(tile);
    }
}
