﻿using UnityEngine;
using System.Collections;
using System;
using UnityEngine.UI;

public class TileView : MonoBehaviour
{
    public Action<Tile> OnClicked;

    [SerializeField]
    private Image _icon;
    private Tile _tile;

    private Button _button;
    public void Initialized(Tile tile)
    {
        if (GetComponent<Button>() == null)
            gameObject.AddComponent<Button>();

        _tile = tile;
        _button = GetComponent<Button>();
        _button.interactable = true;
        _button.onClick.AddListener(() =>
        {
            if (OnClicked != null)
                OnClicked(_tile);
        });

        gameObject.name = tile.Col + " x " + tile.Row;

        UpdateView( tile.State);
    }

    public void UpdateView(TileState state)
    {
        var style = Resources.Instance.GetTileStyle(state);
        _tile.State = state;
        _icon.sprite = style.Icon;
        _icon.color = style.Color;
    }
}
