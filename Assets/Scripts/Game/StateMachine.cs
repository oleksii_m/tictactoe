﻿using System.Collections.Generic;
using UnityEngine;
using System;

namespace StateMachine
{
    public  class StateMachine<T>
    {
        protected T _context;
        public event Action OnStateChanged;

        public State<T> CurrentState { get { return _currentState; } }

        private Dictionary<Type, State<T>> _states = new Dictionary<Type, State<T>>();
        private State<T> _currentState;

        public StateMachine(T context, State<T> initialState)
        {
            this._context = context;
            addState(initialState);
            this._currentState = initialState;

        }

        public void addState(State<T> state)
        {
            state.SetMachineAndContext(this, _context);
            _states[state.GetType()] = state;
        }

        public S ChangeState<S>() where S: State<T>
        {
            var newType = typeof(S);

            if (_currentState != null)
                _currentState.End();

            _currentState = _states[newType];
            _currentState.Begin();

            if (OnStateChanged != null)
                OnStateChanged();

            return _currentState as S;
        }
    }
}
