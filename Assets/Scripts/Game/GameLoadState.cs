﻿using UnityEngine;
using System.Collections;
using StateMachine;

public class GameLoadState : State<IGameState>
{
    private float AIPlayerPossibilitybeWrong;
    public override void OnInitialized()
    {
        var gameDiff = Storage.Instance.Difficulty;
        var AIWrong = gameDiff == (int)Difficulty.Easy ? 0.8f :
            (gameDiff == (int)Difficulty.Normal ? 0.3f : 0.0f);

        AIPlayerPossibilitybeWrong = AIWrong;
    }

    public override void Begin()
    {
        var board = this._context.BoardController;
        var turn = this._context.TurnPlayers;

        var isFirstGame = turn == null;

        if (isFirstGame)
        {
            this._context.TurnPlayers = new TurnPlayers(board,
                new UserPlayer(TileState.Cross, board),
                new AIMinMaxPlayer(TileState.Circle, board, AIPlayerPossibilitybeWrong));
        }
        else
        {
            var players = GetNextTurnPlayers(board, turn);
            this._context.TurnPlayers = new TurnPlayers(board, players[0], players[1]);
        }

        board.Clear();

        this._context.TurnPlayers.OnTurnChange += GameEvent.SetCurrentPlayerTurn;
        this._context.TurnPlayers.StartNextTurn();
        this._machine.ChangeState<GamePlayState>();

        GameEvent.LoadGame();
    }

    Player[] GetNextTurnPlayers(BoardController board, TurnPlayers pastTurn)
    {

        var pasrFirstPlayer = pastTurn.Players[0];
        var pasrSecondPlayer = pastTurn.Players[1];

        var isCrossWinPastGame = board.GetWinerPlayerType() == TileState.Cross;

        if (isCrossWinPastGame)
        {
            return new Player[] { pasrFirstPlayer, pasrSecondPlayer };
        }
        else
        {
            pasrSecondPlayer.Type = TileState.Cross;
            pasrFirstPlayer.Type = TileState.Circle;

            return new Player[] { pasrSecondPlayer, pasrFirstPlayer };
        }
       
    }
}
