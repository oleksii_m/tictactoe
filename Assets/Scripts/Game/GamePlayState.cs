﻿using UnityEngine;
using System.Collections;
using StateMachine;

public class GamePlayState:State<IGameState>
{
    public override void Begin()
    {
        var turn = this._context.TurnPlayers;

        turn.OnGameOver += OnGameOver;
        turn.OnGameOver += GameEvent.GameOver;
    }

    private void OnGameOver(TileState tile)
    {
        var turn = this._context.TurnPlayers;

        turn.OnGameOver -= OnGameOver;
        turn.OnGameOver -= GameEvent.GameOver;
        turn.OnTurnChange -= GameEvent.SetCurrentPlayerTurn;

        var userType = turn.Players[0] as IAIPlayer == null? TileState.Cross: TileState.Circle;
        IncGameState(tile, userType);

        this._machine.ChangeState<GameOverState>();
    }

    private void IncGameState(TileState winer, TileState player)
    {
        var storage = Storage.Instance;
        var isDraw = winer == TileState.None;
        var isPlayerWin = winer == player;

        if (isDraw)
        {
            storage.Draws.IncResult(storage.Difficulty);
        }
        else if(isPlayerWin)
        {
            storage.Wins.IncResult(storage.Difficulty);
        }
        else
        {
            storage.Losses.IncResult(storage.Difficulty);
        }

    }
}
