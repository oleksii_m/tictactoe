﻿using UnityEngine;
using System.Collections;

namespace StateMachine
{
    public abstract class State<T>
    {
        protected StateMachine<T> _machine;
        protected T _context;


        internal void SetMachineAndContext(StateMachine<T> machine, T context)
        {
            _machine = machine;
            _context = context;
            OnInitialized();
        }

        public virtual void OnInitialized(){ }

        public virtual void Begin(){ }

        public virtual void End(){ }
    }
}
