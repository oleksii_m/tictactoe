﻿using UnityEngine;
using System.Collections;
using StateMachine;
public class GameOverState : State<IGameState>
{
    public override void Begin()
    {
        var board = this._context.BoardController;
        GameEvent.OnGameReload += ReloadGame;
    }
    public override void End()
    {
        GameEvent.OnGameReload -= ReloadGame;
    }
    void ReloadGame()
    {
        this._machine.ChangeState<GameLoadState>();
    }
}
