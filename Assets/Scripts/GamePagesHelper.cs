﻿using UnityEngine;
using System.Collections;

public class GamePagesHelper : MonoBehaviour
{
    [SerializeField]
    private GamePlayView _playView;
    [SerializeField]
    private GameOverPageView _overView;

    private GamePlayControler _playControler;
    private GameOverPageControler _overControler;


    private void Awake()
    {
        _playControler = new GamePlayControler(_playView);
        GameEvent.OnGameLoad += _playControler.UpdateGameStats;
        GameEvent.OnPlayerTurnChange += _playControler.UpdateTurnInfo;

        _overControler = new GameOverPageControler(_overView);
        GameEvent.OnGameOver += _overControler.GameOver;
        GameEvent.OnGameReload += _overControler.ReloadGame;

    }

    private void OnDestroy()
    {
        GameEvent.OnGameOver -= _overControler.GameOver;
        GameEvent.OnGameReload -= _overControler.ReloadGame;

        GameEvent.OnGameLoad -= _playControler.UpdateGameStats;
        GameEvent.OnPlayerTurnChange -= _playControler.UpdateTurnInfo;
    }

}
