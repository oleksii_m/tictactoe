﻿using UnityEngine;
using System.Collections;

public class Evaluation
{
    public double Evaluate(Tile[,] board, TileState player)
    {
        if (HasWinner(board))
        {
            if (GetWinerPlayerType(board) == player)
                return double.MaxValue;
            else
                return double.MinValue;
        }

        double maxValue = EvaluateTiles(board, player);
        double minValue = EvaluateTiles(board, Player.GetOponentType(player));

        return maxValue - minValue;
    }

    private double EvaluateTiles(Tile[,] board, TileState player)
    {

        var winTile = Resources.Instance.GetWinTileCombinations();

        double score = 0.0;
        int count;
      
        for (var col = 0; col < winTile.GetLength(0); col++)
        {
            count = 0;
            bool combinationsClean = true;
            for (var row = 0; row < winTile.GetLength(1); row++)
            {
                var tileState = board[(int)winTile[col, row].x, (int)winTile[col, row].y].State;

                if (tileState == player)
                    count++;
                else if (tileState == Player.GetOponentType(player))
                {
                    combinationsClean = false;
                    break;
                }
            }

            if (combinationsClean)
                score += count;
        }

        return score;
    }

    private bool HasWinner(Tile[,] board)
    {
        var winer = GetWinerPlayerType(board);

        return winer != TileState.None && winer != TileState.Empty;
    }

    private TileState GetWinerPlayerType(Tile[,] board)
    {
        var winTile = Resources.Instance.GetWinTileCombinations();

        for (var col = 0; col < winTile.GetLength(0); col++)
        {
            var win = true;
            var startTile = board[(int)winTile[col, 0].x, (int)winTile[col, 0].y];

            if (startTile.IsNullOrEmpty())
                continue;

            for (var row = 1; row < winTile.GetLength(1); row++)
            {
                win &= board[(int)winTile[col, row].x, (int)winTile[col, row].y].State == startTile.State;
            }

            if (win)
                return startTile.State;

        }

        return TileState.None;
    }
}