﻿using UnityEngine;
using System.Collections;
using System;

public abstract class Player
{
    public static TileState GetOponentType(TileState player)
    {
        if (player == TileState.Circle)
            return TileState.Cross;
        else if (player == TileState.Cross)
            return TileState.Circle;

        return player;
    }

    protected BoardController _board;
    public TileState Type { get;  set; }

    public Action<Tile> OnMove;
    public Player(TileState type, BoardController board)
    {
        this.Type = CheckedType(type);
        this._board = board;
    }

    public abstract void DoMove();

    protected TileState CheckedType(TileState type)
    {
        if (type == TileState.None || type == TileState.Empty)
            return TileState.Circle;

        return type;
    }

    protected void Move(Tile check)
    {
        if (OnMove != null)
            OnMove(check);
    }
}
