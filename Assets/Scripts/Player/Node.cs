﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace MinMax
{
    public abstract class Node
    {

        protected List<Node> children;
        protected double value;
        protected Node bestMoveNode;
        protected Tile[,] board;

        protected static Evaluation evaluator;

   
        protected TileState myState;
        protected TileState opponentState; 

        Tile move;
        Node parent = null;

        public Node(Tile[,] board, Node parent, Tile move)
        {
            this.board = board;
            this.parent = parent;
            this.move = move;
            if (parent != null)
                myState = Player.GetOponentType(parent.MyState);
            children = new List<Node>();
        }


        public TileState MyState
        {
            get { return myState; }
            set
            {
                myState = value;
                opponentState = Player.GetOponentType(value);
            }
        }

        public Evaluation Evaluator
        {
            set { evaluator = value; }
        }

        public double Value
        {
            get { return value; }
            set { this.value = value; }
        }



        protected abstract void Evaluate();

        public void SelectBestMove()
        {
            if (children.Count == 0)
            {
                bestMoveNode = null;
                return;
            }
            List<Node> sortedChildren = SortChildren(this.children);

            this.bestMoveNode = sortedChildren[0];
            Value = bestMoveNode.Value;
        }

        public virtual void FindBestMove(int depth)
        {
            if (depth > 0)
            {
               
                GenerateChildren();
                EvaluateChildren();

                bool haveWinningChild = children.Exists(c => c.IsGameEndingNode());

                if (haveWinningChild)
                {
                    SelectBestMove();
                    return;
                }
                else
                {
                    foreach (Node child in children)
                    {
                        child.FindBestMove(depth - 1);
                    }
                    SelectBestMove();
                }
            }

        }

        protected abstract void GenerateChildren();

        public virtual bool IsGameEndingNode()
        {
            return Value == double.MaxValue || Value == double.MinValue;
        }

        public Tile BestMove
        {
            get { return this.bestMoveNode.move; }
        }

        protected void EvaluateChildren()
        {
            foreach (Node child in this.children)
            {
                child.Evaluate();
            }
        }

        protected abstract List<Node> SortChildren(List<Node> unsortedChildren);

        protected abstract bool IsWinningNode();
    }

    public class MaxNode : Node
    {
        public MaxNode(Tile[,] board, Node parent, Tile move)
            : base(board, parent, move)
        {
        }

        protected override void GenerateChildren()
        {
            foreach (var tile in board)
            {
                if (!tile.IsEmpty())
                    continue;

                Tile[,] b = (Tile[,])board.Clone();
                Tile m = new Tile(tile.Col, tile.Row, myState);

                b[tile.Col, tile.Row] = m;
                children.Add(new MinNode(b, this, m));

            }
        }

        protected override void Evaluate()
        {
            this.Value = evaluator.Evaluate(this.board, myState);
        }

        protected override bool IsWinningNode()
        {

            return this.Value == double.MaxValue;
        }

        protected override List<Node> SortChildren(List<Node> unsortedChildren)
        {
            List<Node> sortedChildren = unsortedChildren.OrderByDescending(n => n.Value).ToList();

            return sortedChildren;
        }

    }


    public class MinNode : Node
    {

        public MinNode(Tile[,] b, Node parent, Tile m)
            : base(b, parent, m)
        {

        }

        protected override void GenerateChildren()
        {
            foreach (var tile in board)
            {
                if (!tile.IsEmpty())
                    continue;

                Tile[,] b = (Tile[,])board.Clone();
                Tile m = new Tile(tile.Col, tile.Row, myState);

                b[tile.Col, tile.Row] = m;
                children.Add(new MaxNode(b, this, m));
            }
        }

        protected override bool IsWinningNode()
        {
            return this.value == double.MinValue;

        }

        protected override List<Node> SortChildren(List<Node> unsortedChildren)
        {
            List<Node> sortedChildren = unsortedChildren.OrderBy(n => n.Value).ToList();
            return sortedChildren;
        }
        protected override void Evaluate()
        {
            Value = evaluator.Evaluate(board, Player.GetOponentType(myState));
        }
    }
}
