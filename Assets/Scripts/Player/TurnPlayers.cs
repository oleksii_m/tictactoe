﻿using UnityEngine;
using System.Collections;
using System;

public class TurnPlayers
{
    public event Action<TileState> OnGameOver;
    public event Action<TileState> OnTurnChange;

    public Player[] Players { get { return new Player[] { _first, _second }; } }

    private BoardController _board;
    private Player _first;
    private Player _second;

    private Player _current;

    public TurnPlayers(BoardController board,Player first, Player second)
    {
        this._board = board;
        this._first = first;
        this._second = second;

        _current = first;

    }

    public void StartNextTurn()
    {
        if (OnTurnChange != null)
            OnTurnChange(_current.Type);

        Debug.Log("statr turn " + _current.Type);
        _current.OnMove += MoveComplete;
        _current.DoMove();
    }

    private void MoveComplete(Tile move)
    {
        _current.OnMove -= MoveComplete;
        _board.SetTileState(move);

        if (_board.IsGameOver())
        {
            Debug.Log("GAME OVER " + _board.GetWinerPlayerType());

            if (this.OnGameOver != null)
                this.OnGameOver(_board.GetWinerPlayerType());
            return;
        }

        SwapPlayers();
        StartNextTurn();
    }

    private void SwapPlayers()
    {
        _current = _current.Type == _first.Type ? _second : _first;
    }
}
