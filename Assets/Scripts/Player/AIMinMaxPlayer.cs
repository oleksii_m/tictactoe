﻿using UnityEngine;
using System.Collections;
using MinMax;


public interface IAIPlayer
{

}

public class AIMinMaxPlayer : Player, IAIPlayer
{

    public const int DEFAULT_SEARCH_DEPTH = 4;
    private float _possibilityBeWrong;
    public AIMinMaxPlayer(TileState type, BoardController board, float possibilityBeWrong = 0.0f) : base(type, board)
    {
        this._possibilityBeWrong = possibilityBeWrong;
    }

    public override void DoMove()
    {
        var emptyTile = 0;
        foreach (var tile in _board.GetTiles())
        {
            if (tile.IsEmpty())
                emptyTile++;
        }
        var firstMove = emptyTile == _board.Cols * _board.Rows;
        var wrong = Random.value < _possibilityBeWrong;

        if (firstMove || wrong)
        {
            this.Move(GetRandomMove());
            return;
        }

        Node root = new MaxNode(_board.GetTiles(), null, null);
        root.MyState = this.Type;
        root.Evaluator = new Evaluation();
        root.FindBestMove(DEFAULT_SEARCH_DEPTH);

        Move(root.BestMove);
    }

    protected Tile GetRandomMove()
    {
        var tiles = this._board.GetTiles();
        var col = 0;
        var row = 0;

        do
        {
            col = Random.Range(0, this._board.Cols);
            row = Random.Range(0, this._board.Rows);

        } while (!tiles[col, row].IsEmpty());

        return new Tile(col, row, Type);
    }
}
