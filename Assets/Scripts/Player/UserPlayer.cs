﻿using UnityEngine;
using System.Collections;
using System;

public class UserPlayer : Player
{
    public UserPlayer(TileState type, BoardController board) : base(type, board)
    {
    }

    public override void DoMove()
    {
        var input = this._board.GetInputControler();
        input.OnTilesClicked += TileClicked;

    }

    private void TileClicked(Tile tile)
    {
        if (!tile.IsEmpty())
            return;

        var input = this._board.GetInputControler();
        input.OnTilesClicked -= TileClicked;

        Move(new Tile(tile.Col, tile.Row, this.Type));
    }
}
