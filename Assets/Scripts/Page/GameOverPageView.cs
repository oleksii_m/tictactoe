﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class GameOverPageView : PageView
{
    [SerializeField]
    private Text _winnerDesc;

    public void ReloadGame()
    {
        GameEvent.ReloadGame();
    }

    public void UpdateWinnerType(TileState player)
    {
        var desc = player == TileState.None ? "Draw" : "Winner: " + player.ToString();
        _winnerDesc.text = desc;
    }
}
