﻿using UnityEngine;
using System.Collections;

public class GameOverPageControler : PageControler
{
    public GameOverPageControler(IPageView view) : base(view)
    {
    }

    public void GameOver(TileState player)
    {
        var view = (GameOverPageView)this._view;
        view.gameObject.SetActive(true);

        view.UpdateWinnerType(player);
        this.Open();
    }

    public void ReloadGame()
    {
        this.Close();

        var view = (GameOverPageView)this._view;
        view.gameObject.SetActive(false);
    }
}
