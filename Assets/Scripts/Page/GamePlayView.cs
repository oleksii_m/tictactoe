﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
public class GamePlayView : PageView
{
    [Header("UI"), Space(10), SerializeField]
    private Text _turnDesc;
    [SerializeField]
    private Text _gameStatDesc;
    [SerializeField]
    private Text _defficultyDesc;

    public void UpdateGameStats(string state, string difficulty)
    {
        _defficultyDesc.text = difficulty;
        _gameStatDesc.text = state;
    }

    public void UpdateTurnInfo(TileState turn)
    {
        _turnDesc.text = "Your move: " + turn;
    }

    public void ExitToMainMenu()
    {
        GameEvent.ClearGameReload();
        SceneManager.LoadScene("main");
    }
}
