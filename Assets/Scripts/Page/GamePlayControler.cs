﻿using UnityEngine;
using System.Collections;

public class GamePlayControler : PageControler
{
    public GamePlayControler(GamePlayView view) : base(view)
    {
    }

    public void UpdateGameStats()
    {
        var storage = Storage.Instance;
        var difficulty = storage.Difficulty;
        var gameStats = string.Format("W:{0} D:{1} L:{2}",
            storage.Wins.GetResult(difficulty),
            storage.Draws.GetResult(difficulty),
            storage.Losses.GetResult(difficulty));

        var view = (GamePlayView)_view;
        view.UpdateGameStats(gameStats, ((Difficulty) difficulty).ToString());     
    }

    public void UpdateTurnInfo(TileState turn)
    {
        var view = (GamePlayView)_view;
        view.UpdateTurnInfo(turn);
    }
}
