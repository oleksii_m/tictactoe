﻿using UnityEngine;
using System.Collections;
using System;

public interface IPageView
{
    void Open( );
    void Close( );
}

public enum PageType
{
    None = 0,
    Window,
    Popup
}

public abstract class PageControler 
{
    protected IPageView _view;

    public PageControler(IPageView view)
    {
        this._view = view;
    }

    public virtual void Close()
    {
        _view.Close();
    }

    public virtual void Open()
    {
        _view.Open();
    }
}
