﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.Linq;

public class MainPageView : PageView
{
    [SerializeField]
    private ToggleGroup _toggleGroup;

    public void StartGame()
    {
        var toggles = _toggleGroup.ActiveToggles();
        var toggle = toggles.FirstOrDefault();

        Storage.Instance.Difficulty = toggle.transform.GetSiblingIndex();
        Debug.Log(toggle.transform.GetSiblingIndex());
        SceneManager.LoadScene("game");
    }
    public void ExitGame()
    {
        Application.Quit();
    }
}
