﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Resources : MonoBehaviour
{
    private static Resources _instance;
    public static Resources Instance
    {
        get
        {
            if (_instance == null)
            {
                _instance = FindObjectOfType<Resources>();
            }
            return _instance;
        }
    }

    [SerializeField]
    private List<TileStyle> _tileStyles = new List<TileStyle>();
    private Vector2[,] _winTileCombinations = { { new Vector2(0, 0), new Vector2(0, 1), new Vector2(0, 2) },
                                                { new Vector2(1, 0), new Vector2(1, 1), new Vector2(1, 2) },
                                                { new Vector2(2, 0), new Vector2(2, 1), new Vector2(2, 2) },

                                                { new Vector2(0, 0), new Vector2(1, 0), new Vector2(2, 0) },
                                                { new Vector2(0, 1), new Vector2(1, 1), new Vector2(2, 1) },
                                                { new Vector2(0, 2), new Vector2(1, 2), new Vector2(2, 2) },

                                                { new Vector2(0, 0), new Vector2(1, 1), new Vector2(2, 2) },
                                                { new Vector2(2, 0), new Vector2(1, 1), new Vector2(0, 2) }};

    public Vector2[,] GetWinTileCombinations()
    {
        return _winTileCombinations;
    }

    public TileStyle GetTileStyle(TileState state)
    {
        return _tileStyles.Find(x => x.State == state);
    }
    void Awake()
    {
        _instance = this;
    }

}
